<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">


<div class="alert alert-success" role="alert">
  <h4 class="alert-heading">Merci pour votre confiance !</h4>
  <p>Validation réussi.</p>
  <hr>
  <p class="mb-0">Redirection automatique.</p>
</div>

<?php
  echo '<META HTTP-EQUIV="refresh" CONTENT="3; URL=index.php">'; ?>
