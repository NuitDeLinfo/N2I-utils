<head>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  <link rel="stylesheet" href="style.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<?php
//Créations des variables récupérant les $_POST du formulaire HTML
	$nom = strtolower($_POST['nom']);
	$prenom = strtolower($_POST['prenom']);
	$mail = strtolower($_POST['mail']);
	$password = strtolower($_POST['password']);
	$exist = 0;
  	$confirm_password = strtolower($_POST['confirm_password']);

	//Vérifie que l'utilisateur (en l'occurence, la secrétaire) a bien cliqué sur le bouton d'envoi du formulaire.
	if (!empty($_POST["nom"])
		&& !empty($_POST["prenom"])
		&& !empty($_POST["mail"])
		&& !empty($_POST["password"])
    && !empty($_POST["confirm_password"]))
	{
    if($_POST["password"] == $_POST["confirm_password"]){
      //Connexion à la base de donnée "cabinet".
  		include('connexion_bdd.php');

  		//Préparation de la récupération des données de la table usager, et les stocke dans la variable $res.
  		$res = $linkpdo->prepare('SELECT * FROM organisateur');

  		//Execution de la requête préparée précédemment.
  		$res->execute();

  		//Parcours des données de la table (via la variable $res initialisée plus haut)
  		while($data = $res->fetch()) {
  			//Vérifie l'existence du client entré dans le formulaire d'ajout
  			if($data['mail'] == $mail){
  				print('Adresse email déja prise');
  				$exist = 1;
  			}
  		}
  		//Condition: si exist = 0 (si le client entré dans le formulaire n'existe pas -> Ajout du client dans la base de données)
  		if($exist == 0){
  			//Préparare l'insertion des valeurs du formulaire dans la table usager
  			$req = $linkpdo->prepare('INSERT INTO organisateur(nom, prenom, mail, password)
  			VALUES(:nom, :prenom, :mail, :password)');
  			//Exécution
  			$req->execute(array('nom' => $nom,
        'prenom' => $prenom,
  			'mail' => $mail,
  			'password' => $password));
  			//Redirection vers la page de saisie afin d'ajouter un nouveau client.

  			header('Location: validation_inscription.php');
    }

	} else {
		echo '<div class="alert alert-danger" role="alert">
		  Les mots de passe ne correspondent pas, redirection en cours ...
		</div>';
		echo'<META HTTP-EQUIV="refresh" CONTENT="3; URL=formulaire_organisateur.php">';
	}
}else{
  echo "Veuillez remplir tout les champs";
}
?>
