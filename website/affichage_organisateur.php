<head>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
</head>

<?php
include('header.php');
include('connexion_bdd.php');
$id_evenement = $_POST['id_evenement'];

$res=$linkpdo->prepare('SELECT * FROM event WHERE event.id_event = :id_event');
$linkpdo->exec("SET CHARACTER SET utf8");
$res->execute(array('id_event' => $id_evenement));
while($data = $res->fetch()) {
     $ville = $data['ville'];
     $nom_evenement = $data['nom'];
     $date = $data['date'];
     $adresse = $data['adresse'];
     $activites = $data['activites'];
}
     ?>
     <div class="container jumbotron creer_event">
          <legend>Evenement</legend>
          <div class="form-group">
               <label>Nom evenement</label>
               <input type="text" class="form-control" value="<?php echo $nom_evenement; ?>" name="nom_evenement" >
          </div>
          <div class="form-group">
               <label>Ville</label>
               <input type="text" class="form-control" value="<?php echo $ville; ?>" name="ville" >
          </div>
          <div class="form-group">
               <label for="exampleInputEmail1">Date</label>
               <input type="date" class="form-control" id="exampleInputEmail1" value="<?php echo $date; ?>" aria-describedby="emailHelp" name="date">
          </div>
          <div class="form-group">
               <label for="exampleSelect1">Adresse</label>
               <input type="text" class="form-control" id="exampleInputPassword1" value="<?php echo $adresse; ?>" name="adresse">
          </div>
          <div class="form-group">
               <label for="exampleSelect1">Activités</label>
               <input type="text" class="form-control" id="exampleInputPassword1" value="<?php echo $activites; ?>" name="activites">
          </div>
     </div>
