<!DOCTYPE html>
<html>
     <head>
          <meta charset="utf-8">
          <title></title>
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
          <link rel="stylesheet" href="https://opensource.keycdn.com/fontawesome/4.7.0/font-awesome.min.css ">
          <link rel="stylesheet" href="style.css ">
     </head>
     <body>
     <?php include("header.php") ?>
     <div class="container jumbotron">
      <form class="form-horizontal" method="post" action="traitement_connection.php">
          <fieldset>
               <legend>Connectez-vous !</legend>

               <div class="form-group">
                    <label for="inputEmail" class="col-lg-2 control-label">Email</label>
                    <div class="col-lg-10">
                         <input type="text" class="form-control" id="inputEmail" placeholder="Email" name="mail">
                    </div>
               </div>
               <div class="form-group">
                    <label for="inputPassword" class="col-lg-2 control-label">Password</label>
                    <div class="col-lg-10">
                         <input type="password" class="form-control" id="inputPassword" placeholder="Password" name="password">
                    </div>
               </div>


               <div class="form-group">
                    <div class="col-lg-10 col-lg-offset-2">
                         <a href="formulaire_organisateur.php" class="btn btn-danger">S'inscrire</a>

                         <button type="submit" class="btn btn-primary">Se connecter</button>
                    </div>
               </div>
          </fieldset>
     </form>
     </div>

  </body>
</html>
