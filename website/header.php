<header>
     <nav class="navbar navbar-expand-lg navbar-light bg-dark">
          <a class="navbar-brand text-light" href="index.php">Events Tracker</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
               <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
               <ul class="navbar-nav mr-auto">
               </ul>
               <form class="form-inline my-2 my-lg-0">
                    <a class="nav-link text-light" href="connection.php">Se connecter</a>
                    <a class="nav-link text-light" href="formulaire_organisateur.php">Inscription</a>
               </form>
          </div>
     </nav>
</header>
