-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  ven. 08 déc. 2017 à 06:34
-- Version du serveur :  10.1.26-MariaDB
-- Version de PHP :  7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `evenement`
--

-- --------------------------------------------------------

--
-- Structure de la table `event`
--

CREATE TABLE `event` (
  `ville` varchar(32) NOT NULL,
  `date` date NOT NULL,
  `adresse` varchar(32) NOT NULL,
  `id_event` int(5) NOT NULL,
  `id_organisateur` int(11) NOT NULL,
  `nom` varchar(32) NOT NULL,
  `activites` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `event`
--

INSERT INTO `event` (`ville`, `date`, `adresse`, `id_event`, `id_organisateur`, `nom`, `activites`) VALUES
('Paris', '2017-12-09', '115 Avenue durand ', 1, 1, 'La fiesta', 'Voiture tonneau, code de la route'),
('Toulouse', '2017-12-15', '110 Rue des potiers', 2, 4, 'Nuit sans alcool', 'Alcool au volant, lunettes de vision trouble'),
('Muret', '2017-12-15', '120 avenue jacques douzans', 3, 3, 'Association prÃ©vention Muret', 'Risques à vélo, prévention piétons'),
('Bordeaux', '2015-06-04', '25 chemin des rosiers', 4, 5, 'Nuit de l info', 'Campagne de sensibilisation, quizz sur le risque routier et le permis de conduire et simulation moto');

-- --------------------------------------------------------

--
-- Structure de la table `organisateur`
--

CREATE TABLE `organisateur` (
  `id_organisateur` int(5) NOT NULL,
  `nom` varchar(10) NOT NULL,
  `prenom` varchar(10) NOT NULL,
  `mail` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `organisateur`
--

INSERT INTO `organisateur` (`id_organisateur`, `nom`, `prenom`, `mail`, `password`) VALUES
(1, 'brosse', 'adam', 'brosse.adam@gmail.com', 'brosse'),
(2, 'cabanis', 'georges', 'gcabanis@hotmail.fr', 'cabanis'),
(3, 'alimoux', 'jean', 'alimoujean@hotmail.fr', 'alimou'),
(4, 'bonhomme', 'olivier', 'bolivier@gmail.com', 'bolivier'),
(5, 'jean', 'dupont', 'jdupont@gmail.com', '1234');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id_event`),
  ADD KEY `FK_envent` (`id_organisateur`);

--
-- Index pour la table `organisateur`
--
ALTER TABLE `organisateur`
  ADD PRIMARY KEY (`id_organisateur`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `event`
--
ALTER TABLE `event`
  MODIFY `id_event` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `organisateur`
--
ALTER TABLE `organisateur`
  MODIFY `id_organisateur` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
