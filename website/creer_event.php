<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
<link rel="stylesheet" href="style.css">
<?php include('header.php'); ?>
<div class="container jumbotron creer_event">
     <?php if(isset($_GET['bla'])) {
          ?><div class="alert alert-success" role="alert">
            This is a success alert—check it out!
          </div>
          <?php
     }
          $id_organisateur=$_GET['id_organisateur'];

     ?>
     <form method="post" action="traitement_evenement.php">
          <fieldset>
               <legend>Créer un événement</legend>
               <div class="form-group">
                    <label>Nom événement</label>
                    <input type="text" class="form-control"  name="nom_evenement" placeholder="Ex: Jean-Dupont">
               </div>
               <div class="form-group">
                    <label>Ville</label>
                    <input type="text" class="form-control"  name="ville" placeholder="Ex: Paris">
               </div>
               <div class="form-group">
                    <label for="exampleInputEmail1">Date</label>
                    <input type="date" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="date" placeholder="Ex: 03/03/2010">
               </div>
               <div class="form-group">
                    <label for="exampleSelect1">Adresse</label>
                    <input type="text" class="form-control" id="exampleInputPassword1" name="adresse" placeholder="Ex: 112 Avenue Jacques-Douzans">
               </div>
               <div class="form-group">
                    <label for="exampleSelect1">Activités</label>
                    <input type="text" class="form-control" id="exampleInputPassword1" name="activites" placeholder="Fatigue, alcool, traversée de licornes ...">
               </div>
               <input type="hidden" name="id_organisateur" value="<?php echo $id_organisateur; ?>"/>
               <button type="submit" class="btn btn-primary">Valider</button>
          </fieldset>
     </form>
</div>
